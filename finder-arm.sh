#!/bin/bash

#NEWLOC=`curl -s http://web.audacityteam.org/download/mac | grep -o "http.*audacity-macosx.*\.dmg" -m 1 | sed 's/.\{21\}$//'`

#NEWLOC=`curl -s https://www.audacityteam.org/download/mac/ | grep arm64.dmg | head -1 | awk -F 'href=' '{print $2}' |sed 's/^.//'| sed 's/".*//'`

#NEWLOC=`curl -s https://www.audacityteam.org/download/mac/ | grep arm64.dmg | head -1 | awk -F 'href=' '{print $2}' |sed 's/^.//'| sed 's/".*//'


NEWLOC=`curl https://api.github.com/repos/audacity/audacity/releases/latest 2>/dev/null | /usr/local/izzy/tools/jq -r '.assets[] | .browser_download_url' | grep arm64.dmg`


if [ "x${NEWLOC}" != "x" ]; then
	echo "${NEWLOC}"
fi
